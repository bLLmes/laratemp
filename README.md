## Clone Repo

Follow the step below

- composer install
- cp .env.example .env
- php artisan config:cache
- php artisan key:generate
- php artisan migrate