<div>
    Hello Laratemp Users,
    <br><br>
    To reset your password please click the link below.
    <br><br>
    <a href="{{ $link }}">Password Reset</a>
</div>