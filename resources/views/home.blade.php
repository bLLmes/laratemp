@extends('layout.app')
@section('title', 'Home')

@section('content')
    @include('layout.navbar')

    <br><Br>
    <h2 class="text-center">Welcome to home page</h2>

    <br><br>
    <div class="text-center">
        @can('Abilities Here')
            <p>View User</p>
        @endcan

        @foreach($abilities as $ability)
            <p>{{ $ability }}</p>
        @endforeach
    </div>
@endsection