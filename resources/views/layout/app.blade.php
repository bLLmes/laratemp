<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laratemp | @yield('title')</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;1,100;1,200;1,300;1,400;1,500;1,600&display=swap" rel="stylesheet">

    @yield('css')
    <link href="{{ asset('css/bootstrap/bootstrap.min.css?ver=5.1.2') }}" rel="stylesheet">
    <link href="{{ asset('css/pages/all.css') }}" rel="stylesheet">
</head>
<body>
    @yield('content')

    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js?ver=5.1.2') }}"></script>
    <script src="{{ asset('js/jquery/jquery-3.6.0.min.js') }}"></script>
    @yield('js')
</body>
</html>