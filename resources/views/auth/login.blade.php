@extends('layout.app')
@section('title', 'Login')

@section('css')
    <link href="{{ asset('css/pages/login.css') }}" rel="stylesheet">
@endsection

@section('content')

    <form action="login" method="POST">
        @csrf

        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand-logo">
                        <img src="{{ asset('images/logo/laravel.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @if(Session::has('status'))
                                <div class="alert alert-success">
                                    {{ Session::get('status') }}
                                </div>
                            @endif
                            <h2 class="text-center">Login</h2>
                            <br>
                            <div class="mb-3">
                                <label for="inputEmail" class="form-label">Email</label>
                                <input name="email" type="email" class="form-control" 
                                    id="inputEmail" aria-describedby="emailResponse"
                                    value="{{ old('email') }}" placeholder="Enter Email Address">
                                @if($errors->has('email'))
                                    <div id="emailResponse" class="form-response">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="inputPassword" class="form-label">Password</label>
                                <input name="password" type="password" class="form-control" id="inputPassword" 
                                    aria-describedby="passwordResponse" placeholder="Enter Password">
                                @if($errors->has('password'))
                                    <div id="passwordResponse" class="form-response">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                                <div class="float-end">
                                    <a href="/forgot-password" class="forgot-password">
                                        Forgot Password?
                                    </a>
                                </div>
                            </div>
                            <br><br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary form-control btn-block">Submit</button>
                                <p class="check-account">
                                    Don't have an account? <a href="/register" class="text-center">Register Here</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </form>

@endsection

@section('js')
    <script type="text/javascript">
        setTimeout(function () {
            $('.alert').alert('close');
        }, 4000);
    </script>
@endsection