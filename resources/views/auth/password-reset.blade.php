@extends('layout.app')
@section('title', 'Reset Password')

@section('css')
    <link href="{{ asset('css/pages/login.css') }}" rel="stylesheet">
@endsection

@section('content')

    <form action="/password/reset" method="POST">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}" >
        <input type="hidden" name="email" value="{{ $email }}" >

        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand-logo">
                        <img src="{{ asset('images/logo/laravel.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding-bottom: 20px !important;">
                        <div class="card-body">
                            @if(Session::has('status'))
                                <div class="alert alert-success">
                                    {{ Session::get('status') }}
                                </div>
                            @endif
                            <h2 class="text-center">Reset Password</h2>
                            <br>
                            <div class="mb-3">
                                <label for="inputPassword" class="form-label">
                                    New Password
                                </label>
                                <input name="password" type="password" class="form-control" 
                                    id="inputPassword" aria-describedby="passwordResponse"
                                    value="{{ old('password') }}" placeholder="Enter Password">
                                @if($errors->has('password'))
                                    <div id="passwordResponse" class="form-response">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="inputPasswordConfirmation" class="form-label">Confirm Password</label>
                                <input name="password_confirmation" type="password" class="form-control" id="inputPasswordConfirmation" 
                                    aria-describedby="passwordConfirmationResponse" placeholder="Confirm Password">
                                @if($errors->has('password_confirmation'))
                                    <div id="passwordConfirmationResponse" class="form-response">
                                        {{ $errors->first('password_confirmation') }}
                                    </div>
                                @endif
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary form-control btn-block">Change password</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </form>

@endsection

@section('js')

@endsection