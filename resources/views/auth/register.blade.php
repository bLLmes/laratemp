@extends('layout.app')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap-select.min.css?ver=1.14.0-beta2') }}" />
    <link href="{{ asset('css/pages/login.css') }}" rel="stylesheet">
@endsection

@section('content')

    <form action="register" method="POST">
        @csrf

        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand-logo">
                        <img src="{{ asset('images/logo/laravel.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            @if(Session::has('status'))
                            <div class="alert alert-success">
                                {{ Session::get('status') }}
                            </div>
                            @endif
                            <h2 class="text-center">Register</h2>
                            <br>
                            <div class="mb-3">
                                <label for="inputName" class="form-label">
                                    Name 
                                    <span class="input-required">*</span>
                                </label>
                                <input name="name" type="text" class="form-control" 
                                    id="inputName" aria-describedby="nameResponse"
                                    value="{{ old('name') }}" placeholder="Enter Name">
                                @if($errors->has('name'))
                                    <div id="nameResponse" class="form-response">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="inputEmail" class="form-label">
                                    Email 
                                    <span class="input-required">*</span>
                                </label>
                                <input name="email" type="email" class="form-control" 
                                    id="inputEmail" aria-describedby="emailResponse"
                                    value="{{ old('email') }}" placeholder="Enter Email">
                                @if($errors->has('email'))
                                    <div id="emailResponse" class="form-response">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="selectRole" class="form-label">
                                    Roles 
                                    <span class="input-required">*</span>
                                </label>
                                <select class="selectpicker form-control" multiple aria-label="User Role"
                                    id="selectRole" aria-describedby="selectResponse"
                                    name="roles[]">
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('roles'))
                                    <div id="selectResponse" class="form-response">
                                        {{ $errors->first('roles') }}
                                    </div>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="inputPassword" class="form-label">
                                    Password 
                                    <span class="input-required">*</span>
                                </label>
                                <input name="password" type="password" class="form-control" id="inputPassword" 
                                    aria-describedby="passwordResponse" placeholder="Enter Password">
                                @if($errors->has('password'))
                                    <div id="passwordResponse" class="form-response">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="inputPasswordConfirmation" class="form-label">Confirm Password</label>
                                <input name="password_confirmation" type="password" class="form-control" id="inputPasswordConfirmation" 
                                    aria-describedby="passwordConfirmationResponse" placeholder="Confirm Password">
                                @if($errors->has('password_confirmation'))
                                    <div id="passwordConfirmationResponse" class="form-response">
                                        {{ $errors->first('password_confirmation') }}
                                    </div>
                                @endif
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary form-control btn-block">Submit</button>
                                <p class="check-account">
                                    Already have an account? <a href="/login" class="text-center">Login Here</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('js')
    <script src="{{ asset('js/bootstrap/bootstrap-select.min.js?ver=1.14.0-beta2') }}"></script>
    <script type="text/javascript">
        $('select').selectpicker();
        
        setTimeout(function () {
            $('.alert').alert('close');
        }, 4000);
    </script>
@endsection