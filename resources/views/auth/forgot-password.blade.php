@extends('layout.app')
@section('title', 'Forgot Password')

@section('css')
    <link href="{{ asset('css/pages/login.css') }}" rel="stylesheet">
@endsection

@section('content')

    <form action="/forgot-password" method="POST">
        @csrf
        <input type="hidden" name="token">

        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand-logo">
                        <img src="{{ asset('images/logo/laravel.png') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding-bottom: 0px !important;">
                        <div class="card-body">
                            @if(Session::has('status'))
                                <div class="alert alert-success">
                                    {{ Session::get('status') }}
                                </div>
                            @endif
                            <h2 class="text-center">Forgot Password</h2>
                            <br>
                            <div class="mb-3">
                                <label for="inputEmail" class="form-label text-center">
                                    Please enter your Email so we can send you an email to reset your password.
                                </label>
                                <input name="email" type="email" class="form-control" 
                                    id="inputEmail" aria-describedby="emailResponse"
                                    value="{{ old('email') }}" placeholder="Enter Email Address">
                                @if($errors->has('email'))
                                    <div id="emailResponse" class="form-response">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary form-control btn-block">Send reset email</button>
                                <p class="check-account">
                                    <a href="/register" class="text-center">Go Back</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </form>

@endsection

@section('js')
    <script type="text/javascript">
        setTimeout(function () {
            $('.alert').alert('close');
        }, 4000);
    </script>
@endsection