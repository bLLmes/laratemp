<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;

class PasswordReset extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'token',
    ];

    public function setTokenAttribute($value)
    {
        $this->attributes['token'] = Str::random(40);
    }

    public function setUpdatedAt($value)
    {
        return null;
    }

    public static function checkToken($token, $email)
    {
        return self::where('token', $token)->where('email', $email)->first();
    }
}
