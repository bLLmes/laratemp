<?php

namespace App\Mail\Auth;

use App\Models\Auth\PasswordReset as AuthPasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AuthPasswordReset $passwordReset)
    {
        $this->token = $passwordReset->token;
        $this->email = $passwordReset->email;
        $this->link = url("password/reset/{$this->token}?email={$this->email}");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.password-reset')
            ->with([
                'link' => $this->link,
                'link' => $this->link,
            ]);
    }
}
