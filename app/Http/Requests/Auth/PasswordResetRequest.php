<?php

namespace App\Http\Requests\Auth;

use App\Mail\Auth\PasswordReset as MailPasswordReset;
use App\Models\Auth\PasswordReset;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rules\Password;

class PasswordResetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => [
                'required', 'confirmed', 
                Password::min(5)
            ],
        ];
    }

    public function store($request)
    {
        $user = User::where('email', $request->email)->first();
        $user->password = $request->password;
        return $user->save();
    }
}
