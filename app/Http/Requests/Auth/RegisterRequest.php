<?php

namespace App\Http\Requests\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email|min:13|unique:users',
            'roles' => 'required',
            'password' => [
                'required', 'confirmed', 
                Password::min(5)
                    // ->mixedCase()
                    // ->numbers()
                    // ->symbols()
                    // ->uncompromised()
            ],
        ];
    }

    public function store()
    {
        $user = User::create($this->except('roles', '_token', 'password_confirmation'));
        return $user->assignRole($this->only('roles')['roles']);
    }

}
