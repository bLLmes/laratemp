<?php

namespace App\Http\Requests\Auth;

use App\Mail\Auth\PasswordReset as MailPasswordReset;
use App\Models\Auth\PasswordReset;
use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users',
            'token' => '',
        ];
    }
    
    public function messages()
    {
        return [
            'email.exists:users' => 'Email does not exist'
        ];
    }

    public function store()
    {
        $data = PasswordReset::create($this->validated());
        Mail::to($data->email)->send(new MailPasswordReset($data));
        return $data;
    }
}
