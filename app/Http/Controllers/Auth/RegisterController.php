<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\Role;

class RegisterController extends Controller
{
    public function show()
    {
        $this->data['roles'] = Role::orderBy('name', 'asc')->get();
        return view('auth.register', $this->data);
    }

    public function store(RegisterRequest $request)
    {
        $request->store();
        return redirect('/login')->with('status', 'Registration Complete!');
    }
}
