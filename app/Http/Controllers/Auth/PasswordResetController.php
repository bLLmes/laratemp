<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\PasswordResetRequest;
use App\Models\Auth\PasswordReset;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

use Illuminate\Support\Facades\Hash;

class PasswordResetController extends Controller
{
    public function show($token, Request $request)
    {
        if (!PasswordReset::checkToken($token, $request->email))
        {
            return 'Token expired';
        }

        $this->data['token'] = $token;
        $this->data['email'] = $request->email;
        return view('auth.password-reset', $this->data);
    }

    public function store(PasswordResetRequest $request)
    {
        if (!PasswordReset::checkToken($request->token, $request->email))
        {
            return 'Token expired';
        }

        $request->store($request);
        return redirect('/login')->with('status', 'Password changed successfully!');
    }
}
