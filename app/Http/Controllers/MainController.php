<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class MainController extends Controller
{
    public function __construct(Gate $gate)
    {
        // apply this for specific module controller only
        // $this->middleware('can:View About');
    }

    public function home()
    {
        $this->data['abilities'] = Auth::user()->roles->map->abilities->flatten()->pluck('name')->unique();
        return view('home', $this->data);
    }

    public function about()
    {
        if (!Gate::allows('View About'))
        {
            return redirect()->back();
        }
        return view('about');
    }
}