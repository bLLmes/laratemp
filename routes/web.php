<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\PasswordResetController;
use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;

// User route start here
Route::middleware(['guest'])->group(function()
{
    Route::get('/login', [LoginController::class, 'show']);
    Route::post('/login', [LoginController::class, 'login']);

    Route::get('/register', [RegisterController::class, 'show']);
    Route::post('/register', [RegisterController::class, 'store']);

    Route::get('/forgot-password', [ForgotPasswordController::class, 'show']);
    Route::post('/forgot-password', [ForgotPasswordController::class, 'store']);

    Route::get('/password/reset/{token}', [PasswordResetController::class, 'show']);
    Route::post('/password/reset', [PasswordResetController::class, 'store']);
});

Route::middleware(['auth'])->group(function()
{
    Route::get('/home', [MainController::class, 'home']);
    Route::get('/about', [MainController::class, 'about']);
        // ->middleware('can:View About');

    Route::post('/logout', [LoginController::class, 'logout']);
});
// User route end here